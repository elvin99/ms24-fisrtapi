package com.elvin.ms24.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/ms24")
@RestController
public class Ms24Controller {

    @GetMapping
    public String sayHello(){
        return "Hello World";
    }
}
