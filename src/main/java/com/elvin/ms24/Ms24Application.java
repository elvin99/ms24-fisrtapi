package com.elvin.ms24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms24Application {

    public static void main(String[] args) {
        SpringApplication.run(Ms24Application.class, args);
    }

}
